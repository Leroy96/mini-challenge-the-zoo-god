// DOM Elements
let zooListElement = document.getElementById('zoo-list')
const emojiInputElement = document.getElementById('emoji')
const nameInputElement = document.getElementById('name')
const buttonElement = document.getElementById('add-animal')

// Functional constructor
function Animal(id, name, emoji) {
	this.id = id
	this.name = name
	this.emoji = emoji
}

// Render function
const addListElement = (name, emoji) => {
	let listItem = document.createElement('li')
	// listItem.appendChild(document.createTextNode(emoji + ' ' + name))
	listItem.innerText = emoji + ' ' + name
	zooListElement.appendChild(listItem)
}

// Functions
const addAnimalToList = () => {
	let emojiInputValue = emojiInputElement.value
	let nameInputValue = nameInputElement.value

	console.log('Added a new animal: ' + nameInputValue + ', that looks like is:' + emojiInputValue)

	const animal = new Animal(Math.random(), nameInputValue, emojiInputValue)

	addListElement(animal.name, animal.emoji)

	emojiInputElement.value = ''
	nameInputElement.value = ''
}

// Event listener
buttonElement.addEventListener('click', addAnimalToList)
